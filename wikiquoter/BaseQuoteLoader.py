#!/usr/bin/env python

"""
    Name
    ~~~~~~~~~

    Description.

    :copyright: (c) 2017 by Duncan Dickinson.
    :license: Apache License 2.0, see LICENCE for more details.
"""
from abc import ABCMeta, abstractmethod, abstractproperty

import requests


class BaseQuoteLoader(object):
    """Description

    """

    __metaclass__ = ABCMeta

    def __init__(self, name):
        self._name = name
        self._quotes = []

    @staticmethod
    def base_url():
        """The base URL for the Wikiquote API"""
        return 'http://en.wikiquote.org/w/api.php'

    @property
    def name(self):
        return self._name

    @property
    def quotes(self):
        return self._quotes

    @abstractmethod
    def load_quotes(self):
        """Loads all of the quotes for the object"""

    @abstractproperty
    def quote_type(self):
        """String denoting the type of quote"""

    def __repr__(self):
        return 'Name: {}, Quotes: {}'.format(self.name,
                                             len(self.quotes))

    @classmethod
    def query_api(cls, text, segments):
        """Queries the API
        :param text:
        :param segments:
        :return:
        """
        return requests.get(cls.base_url(),
                            params={'format': 'json',
                                    'action': 'parse',
                                    'page': text.replace(' ', '_'),
                                    'prop': segments}).json()
