"""
    wikiquoter.__main__
    ~~~~~~~~~~~~~~
    Alias for wikiquoter.run for the command line.
    :copyright: (c) 2017 Duncan Dickinson
    :license: BSD, see LICENSE for more details.
"""


if __name__ == '__main__':
    from .cli import main
    main(as_module=True)
