#!/usr/bin/env python

"""
    wikiquoter.cli
    ~~~~~~~~~



    :copyright: (c) 2017 by Duncan Dickinson.
    :license: Apache License 2.0, see LICENCE for more details.
"""
import sys


def main(as_module=False):
    this_module = __package__ + '.cli'
    args = sys.argv[1:]

    if as_module:
        if sys.version_info >= (2, 7):
            name = 'python -m ' + this_module.rsplit('.', 1)[0]
        else:
            name = 'python -m ' + this_module

        sys.argv = ['-m', this_module] + sys.argv[1:]
    else:
        name = None


if __name__ == '__main__':
    main(as_module=True)
