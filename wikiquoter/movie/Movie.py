#!/usr/bin/env python

"""
    Name
    ~~~~~~~~~

    Description.

    :copyright: (c) 2017 by Duncan Dickinson.
    :license: Apache License 2.0, see LICENCE for more details.
"""
import pprint

from bs4 import BeautifulSoup

from wikiquoter.BaseQuoteLoader import BaseQuoteLoader
from wikiquoter.quote import Quote


class Movie(BaseQuoteLoader):
    """Description

    """

    @property
    def quote_type(self):
        return 'Movie'

    def load_quotes(self):
        """

        :param max_seasons: 0 if all seasons, positive number otherwise
        :return:
        """

        result = self.query_api(self.name, 'text')

        soup = BeautifulSoup(result['parse']['text']['*'], 'html.parser')

        for q in soup.find_all('dl'):
            self.quotes.append(Quote(self.quote_type,
                                     self.name.encode('utf-8'),
                                     '',
                                     q.encode('utf-8')))

        return self.quotes

movie = Movie('Star Wars (film)')
movie.load_quotes()
print(movie)
for q in movie.quotes:
    pprint.pprint(q)
