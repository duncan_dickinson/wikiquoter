#!/usr/bin/env python

"""
    Name
    ~~~~~~~~~

    Description.

    :copyright: (c) 2017 by Duncan Dickinson.
    :license: Apache License 2.0, see LICENCE for more details.
"""
import pprint

import requests
from bs4 import BeautifulSoup

from wikiquoter.BaseQuoteLoader import BaseQuoteLoader
from wikiquoter.quote import Quote


class Person(BaseQuoteLoader):
    """Description

    """

    @property
    def quote_type(self):
        return 'TV Show'

    def load_quotes(self):
        """

        :param max_seasons: 0 if all seasons, positive number otherwise
        :return:
        """

        result = self.query_api(self.name, 'text')

        soup = BeautifulSoup(result['parse']['text']['*'], 'html.parser', from_encoding="utf-8")
        quotes = soup.ul.li

        for q in quotes:
            self.quotes.append(Quote(self.quote_type,
                                       self.name.encode('utf-8'),
                                       '',
                                       q.encode('utf-8')))

        return self.quotes

person = Person('Jim Henson')
person.load_quotes()
print(person)
for q in person.quotes:
    pprint.pprint(q)
