#!/usr/bin/env python

"""
    Name
    ~~~~~~~~~

    Description.

    :copyright: (c) 2017 by Duncan Dickinson.
    :license: Apache License 2.0, see LICENCE for more details.
"""
from collections import OrderedDict, namedtuple

#Quote = namedtuple('Quote', ['attribution', 'context', 'body'], verbose=True)
from operator import itemgetter


class Quote(tuple):
    """Description

    """

    __slots__ = ()

    _fields = ('type', 'attribution', 'context', 'body')

    def __new__(_cls, type, attribution, context, body):
        return tuple.__new__(_cls, (type, attribution, context, body))

    @classmethod
    def _make(cls, iterable, new=tuple.__new__, len=len):
        result = new(cls, iterable)
        if len(result) != 2:
            raise TypeError('Expected 2 arguments, got %d' % len(result))
        return result

    def __repr__(self):
        return 'Quote(%s, %s, %s, %s)' % self

    def _asdict(self):
        return OrderedDict(zip(self._fields, self))

    def _replace(_self, **kwds):
        'Return a new Quote object replacing specified fields with new values'
        result = _self._make(map(kwds.pop, _self._fields, _self))
        if kwds:
            raise ValueError('Got unexpected field names: %r' % kwds.keys())
        return result

    def __getnewargs__(self):
        'Return self as a plain tuple.  Used by copy and pickle.'
        return tuple(self)

    __dict__ = property(_asdict)

    def __getstate__(self):
        'Exclude the OrderedDict from pickling'
        pass

    type = property(itemgetter(0), doc='The quote type')
    attribution = property(itemgetter(1), doc='The quote\'s attribution')
    context = property(itemgetter(2), doc='The quote\'s context')
    body = property(itemgetter(3), doc='The quote itself')
