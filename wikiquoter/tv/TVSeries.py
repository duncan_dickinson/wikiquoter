#!/usr/bin/env python

"""
    Name
    ~~~~~~~~~

    Description.

    :copyright: (c) 2017 by Duncan Dickinson.
    :license: Apache License 2.0, see LICENCE for more details.
"""
import pprint
from operator import itemgetter

import requests
from bs4 import BeautifulSoup

from wikiquoter.BaseQuoteLoader import BaseQuoteLoader
from wikiquoter.quote import Quote


class TVSeries(BaseQuoteLoader):
    """Description

    """

    def __init__(self, name):
        super(TVSeries, self).__init__(name)
        self._seasons = []

    @property
    def quote_type(self):
        return 'TV Show'

    def load_quotes(self, max_seasons=0):
        """

        :param max_seasons: 0 if all seasons, positive number otherwise
        :return:
        """
        self._seasons = self.__load_tv_series()
        season_count = 0
        for season in self.seasons:
            self._quotes.extend(self.__load_season_quotes(season))
            season_count += 1
            if season_count == max_seasons:
                break

    def __repr__(self):
        return 'Series: {}, Seasons: {}, Quotes: {}'.format(self.name,
                                                            len(self._seasons),
                                                            len(self.quotes))

    @property
    def seasons(self):
        return self._seasons

    def __load_tv_series(self):
        def name_checker(v):
            return v.startswith(self.name.replace('_', ' ')) and not v.endswith(' (disambiguation)')

        result = self.query_api(self.name, 'links')
        return filter(name_checker, list(map(itemgetter('*'), result['parse']['links'])))

    def __load_season_quotes(self, season):
        season_quotes = []
        result = self.query_api(season, 'text')

        soup = BeautifulSoup(result['parse']['text']['*'], 'html.parser')
        quotes = soup.find_all('dl')

        for q in quotes:
            #pprint.pprint(''.join(q.dd.stripped_strings))
            text_arr = q.find_all('dd')
            pprint.pprint(text_arr)
            text = text_arr
            season_quotes.append(Quote(self.quote_type,
                                       self.name.encode('utf-8'),
                                       season.encode('utf-8'),
                                       text))

        return season_quotes


show = TVSeries('Knight Rider')
show.load_quotes(1)
print(show)
for q in show.quotes:
    pprint.pprint(q)
